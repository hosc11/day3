## Prepare project structure
1. Create a project directory `mkdir <project name>`
2. Create directory, named: server under project directory `mkdir <project name>/server`
3. Create directory, named: client under project directory `mkdir <project name>/client`

## Setup Bitbucket
1. Create a new repository 
2. Check or uncheck for [This is a private repository] to choose the access level

## Prepare Git
1. git init
2. create .gitignore e.g.: node_modules 
3. git remote -v --> to check the current remote directory
4. git remote remove origin --> if point to the wrong origin
5. git remote add origin <http git URL> --> to add remote origin, , e.g.:  git remote add origin https://hosc11@bitbucket.org/hosc11/day3.git
6. git add . --> add files to staging area
7. git commit -m "xxxxxx"  --> commit files to local repository, e.g: git commit -m "Initial comment" 
8. git push -u origin master --> to push local repository to remote repository

## Global Utility installation
1. `npm install -g nodemon`
2. `npm install -g bower`
or
3. `npm install -g nodemon bower`

## Prepare Express JS
1. npm init
2. npm install express --save

## How to start my app
1. export NODE_PORT=3000
2. nodemon or nodemon server/app.js
3. at browser: localhost:3000

## Notes
1. Npm and Bower are both 'dependency management tools. But the main difference between both is npm is used for installing Node js modules but bower js is used for managing front end components like html, css, js etc.

`bower install angular --save`
`bower install bootstrap --save`
--> --save will save the dependency in bower.json

